## Simple PHP Example

This is an overly simplified version of an app I might build. In reality, I would use React (or something similar) for the front end. I might go with Laravel or another library for PHP backend. Currently, there is no MySQL adapter, so data is not stored anywhere. I built this in about 1 hour and 15 minutes (first commit + Readme commit).

### To Run

Simply run `./run-dev.sh` and travel to http://localhost:8100/.


