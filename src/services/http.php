<?php
namespace App\Services;

class HTTP {
  static function error(String $message, int $statusCode = 400) {
    \http_response_code($statusCode);
    HTTP::send($message);
  }

  static function send(Array $message) {
    header('Content-Type: application/json; charset=UTF-8');
    echo \json_encode([
      'response' => $message,
    ]);
    die();
  }

  static function input(String $key = null) {
    $data = json_decode(file_get_contents('php://input'), true);
    if (!isset($key)) {
      return $data;
    }
    else if (isset($data[$key])) {
      return $data[$key];
    }
    else {
      return null;
    }
  }
}
?>