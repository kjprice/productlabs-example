<?php
  include('../../api-init.php');
  use App\models\User;
  use App\Services\HTTP;

  $userData = HTTP::input('user');

  if (!isset($userData)) {
    HTTP::error('user is required');
  }

  $user = new User($userData['firstName']);
  $user->save();

  HTTP::send($userData);

?>